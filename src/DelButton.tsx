import React, { useState, ReactNode } from 'react';
import { Popconfirm, Button, ButtonProps } from 'antd';
import SvgIcon from './SvgIcon';

interface Props {
  onConfirm?: () => void;
  loading: boolean;
  onBeforeVisible: () => boolean;
  title: ReactNode;
  buttonProps?: ButtonProps;
}

/** 弹窗确认删除按钮
 * button的属性和样式与PopButton不同，暂时未作为同一个组件处理
 */
const DelButton = ({ onConfirm, loading, onBeforeVisible, title, buttonProps }: Props) => {
  const [visible, setVisible] = useState(false);
  const [buttonLoading, setButtonLoading] = useState(false);
  return (
    <Popconfirm
      title={title}
      open={visible}
      disabled={buttonProps?.disabled}
      onOpenChange={(v) => {
        if (!v) {
          setVisible(false);
          setButtonLoading(false);
          return;
        }
        setVisible(onBeforeVisible());
      }}
      onConfirm={async () => {
        try {
          setButtonLoading(true);
          await onConfirm?.();
        } catch (error) {
          console.error(error);
        }
      }}
      okButtonProps={{ danger: true, loading: buttonLoading || loading }}
    >
      <Button loading={buttonLoading || loading} icon={<SvgIcon name="icon-delete" />} {...buttonProps}>
        删除
      </Button>
    </Popconfirm>
  );
};
DelButton.defaultProps = {
  title: '确认删除已选订单吗？',
  onBeforeVisible() {
    return true;
  },
};
export default DelButton;
