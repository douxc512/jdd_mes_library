import React from 'react';
import { valueNotNullAndUndefined } from './utils';
import { Typography } from 'antd';

interface Props {
  value?: string | number | null;
  className?: string;
  style?: Record<string, any>;
  ellipsis?: boolean;
  formatter?: (v: Props['value']) => string | null;
}
/** 直接展示传入的值，目前用于表单展示固定值 */
export default function Text({ value, style, className, ellipsis, formatter }: Props) {
  // 格式化值
  function format(value: string | number) {
    if (formatter) {
      return formatter(value);
    } else {
      return `${value}`;
    }
  }
  return ellipsis === false ? (
    <span className="break-all">{valueNotNullAndUndefined(value) ? format(value!) : '-'}</span>
  ) : (
    <Typography.Text
      className={`${className ?? ''} text-current`}
      ellipsis={{ tooltip: valueNotNullAndUndefined(value) ? `${value}` : '-' }}
      style={{ ...style }}
    >
      {valueNotNullAndUndefined(value) ? format(value!) : '-'}
    </Typography.Text>
  );
}
