import { Image } from 'antd';
import React, { useState } from 'react';

/**
 * 图片预览，支持多张
 * 多张时，默认展示第一张，并显示共n张
 * */
interface ImagePreviewProps {
  urls: string[];
}
export default function ImagePreview({ urls }: ImagePreviewProps) {
  const [visible, setVisible] = useState(false);
  return (
    <div className="inline-block">
      <Image.PreviewGroup preview={{ visible, onVisibleChange: (v) => setVisible(v) }}>
        {urls.map((url, index) =>
          index === 0 ? (
            <Image key={`${index}_${url}`} src={url} width={40} height={40} />
          ) : (
            <div key={`${index}_${url}`} style={{ display: 'none' }}>
              <Image src={url} width={40} height={40} />
            </div>
          ),
        )}
      </Image.PreviewGroup>
      {urls.length > 1 ? (
        <p
          className="text-primary cursor-pointer"
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
            setVisible(true);
          }}
        >
          (共{urls.length}张)
        </p>
      ) : null}
    </div>
  );
}
