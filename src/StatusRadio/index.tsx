import './index.css';
import React from 'react';
import { Tabs, TabsProps } from 'antd';

interface StatusRadioProps {
  value?: number;
  defaultCheck?: string | number;
  options: { label: string; value: string | number }[];
  onChange?: (v: StatusRadioProps['value']) => void;
  external?: TabsProps['tabBarExtraContent'];
}
/**
 * @deprecated
 * 列表统一的tab切换
 */
const StatusRadio = function ({ value, options, defaultCheck, external, onChange }: StatusRadioProps) {
  console.warn('BasePage 集成了StatusRadio，该组件可以安全移除了');
  return (
    <Tabs
      className="status-radio w-full"
      // 此处value类型强制为string，实际使用会转回number
      activeKey={value?.toString()}
      defaultActiveKey={defaultCheck?.toString()}
      onChange={(val) => {
        onChange?.(Number(val));
      }}
      tabBarExtraContent={external}
      items={options.map((op) => ({ ...op, key: `${op.value}` }))}
    />
  );
};

export default StatusRadio;
