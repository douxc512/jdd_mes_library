// 面积格式化
import React from 'react';
import { valueNotNullAndUndefined } from './utils';
import { Tooltip } from 'antd';

/**
 * 格式化面积之后展示
 * @param param0 需要格式化的面积
 * @returns 保留2位小数的面积
 */
interface Props {
  value?: number | null;
  className?: string | null;
}
const Area = function ({ value, className }: Props) {
  if (valueNotNullAndUndefined(value)) {
    const val = Number(value!.toFixed(2));
    return (
      <Tooltip title={value}>
        <span className={`${className}`}>{val}</span>
      </Tooltip>
    );
  } else {
    return <span>-</span>;
  }
};
export default Area;
