import React from 'react';
import { EditOutlined } from '@ant-design/icons';
import { Input, Popover, Typography } from 'antd';

export default function Remark({ value, onChange }: React.TextareaHTMLAttributes<HTMLTextAreaElement>) {
  return (
    <Popover
      trigger="click"
      destroyTooltipOnHide
      overlayInnerStyle={{ paddingBottom: '16px' }}
      content={<Input.TextArea autoFocus rows={4} showCount maxLength={200} value={value} onChange={onChange} />}
    >
      <div className="w-80px inline-flex items-center justify-center">
        <Typography.Text ellipsis>{value && <span style={{ marginRight: 5 }}>{value}</span>}</Typography.Text>
        <span className="text-primary">
          <EditOutlined />
        </span>
      </div>
    </Popover>
  );
}
