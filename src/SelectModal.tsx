import React, { Key, ReactNode } from 'react';
import { Button, Col, Modal, ModalProps, Pagination, PaginationProps, Row, Space, Typography } from 'antd';
import { InfoCircleFilled } from '@ant-design/icons';

interface Props extends ModalProps {
  selectedRowKeys: Key[];
  children: ReactNode;
  onSelectAll?: () => void;
  pagination: PaginationProps | boolean;
}

const SelectModal = function ({
  children,
  pagination,
  onCancel,
  onOk,
  selectedRowKeys,
  okButtonProps,
  open,
  okText,
  cancelText,
  ...restProps
}: Props) {
  return (
    <Modal
      open={open}
      centered
      width={960}
      onCancel={onCancel}
      footer={
        <Row justify="space-between" align="middle">
          <Col>
            <Typography.Text className="min-w-120px text-left inline-block">
              <span className="text-primary mr-5px">
                <InfoCircleFilled />
              </span>
              <span>已选{selectedRowKeys.length}项</span>
            </Typography.Text>
          </Col>
          {pagination ? (
            <Col>
              <Pagination showQuickJumper showSizeChanger {...(pagination as Record<string, any>)} />
            </Col>
          ) : null}
          <Col>
            <Space>
              <Button
                onClick={(e) => {
                  onCancel?.(e);
                }}
              >
                {cancelText}
              </Button>
              <Button
                {...okButtonProps}
                type="primary"
                onClick={(e) => {
                  onOk?.(e);
                }}
              >
                {okText}
              </Button>
            </Space>
          </Col>
        </Row>
      }
      {...restProps}
    >
      {children}
    </Modal>
  );
};
SelectModal.defaultProps = {
  cancelText: '关闭',
  okText: '添加',
};
export default SelectModal;
