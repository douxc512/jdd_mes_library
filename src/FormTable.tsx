import React, { FC, ReactElement, useCallback } from 'react';
import { valueNotNullAndUndefined } from './utils';
import { Empty, Form } from 'antd';
import { ValidatorRule } from 'rc-field-form/lib/interface';
import Text from './Text';

interface Column {
  required?: boolean;
  /** 指定宽度 如 96px */
  width?: string;
  title: string;
  hidden?: boolean;
  /** 当前<Form.Item/> 绑定的name */
  dataIndex: string;
  /** 当前column渲染函数，如果需要绑定到form.list中必须返回<Form.Item />，处理list下的绑定关系 */
  render?: (
    field: { name: number; key: number },
    operation: {
      add: (defaultValue?: any, insertIndex?: number) => void;
      remove: (index: number | number[]) => void;
      move: (from: number, to: number) => void;
    },
    fields?: any,
  ) => ReactElement;
  children?: Column[];
}
interface Props<T> {
  rules?: ValidatorRule[];
  /** 顶部head配置 */
  columns: Array<Column>;
  /** Form.List 绑定的name值 */
  formListName: string;
  /** 需要隐藏的字段，因为hidden字段不展示、不能输入，所以直接做默认处理即可 */
  hiddenFields?: string[];
  /** 初始值 */
  initialValue?: any[];
}
/**
 * 表格样式的form
 * @param props
 * @returns React.ReactElement
 */
const FormTable: FC<Props<Record<string, any>>> = function ({
  columns,
  formListName,
  hiddenFields,
  rules,
  initialValue,
}) {
  /** 没有配置渲染函数时，默认直接展示值 */
  function defaultColumnRender(index: number, column: Column) {
    return (
      <Form.Item wrapperCol={{ span: 24 }} name={[index, column['dataIndex']]} className="mb-0">
        <Text style={{ width: column.width ?? 'auto' }} />
      </Form.Item>
    );
  }

  /** 渲染form-item部分 */
  const defaultColumnsRender = useCallback(
    (
      field: { name: number; key: number },
      operate: {
        add: (defaultValue?: any, insertIndex?: number) => void;
        remove: (index: number | number[]) => void;
        move: (from: number, to: number) => void;
      },
      fields?: any,
    ) => {
      function render(column: Column, index: number) {
        return (
          <td
            hidden={Boolean(column.hidden)}
            style={{ width: column.width }}
            className="p-5px  text-center align-top"
            key={`${field.key}_${index}_${column.dataIndex}`}
          >
            {column.render?.(field, operate, fields) ?? defaultColumnRender(field.name, column)}
          </td>
        );
      }
      return columns
        .filter((column) => !column.hidden)
        .map((column, index) => {
          return column.children ? column.children.map(render) : render(column, index);
        });
    },
    [columns],
  );

  return (
    <table className="w-full">
      <thead className="leading-32px text-center bg-hex-f2f3f5 text-black/85 border border-1px ">
        {renderComplexColumn(columns)}
      </thead>
      <Form.List name={formListName} rules={rules} initialValue={initialValue}>
        {(fields, operate, { errors }) => {
          return (
            <tbody>
              {fields && fields.length > 0 ? (
                fields.map((field) => {
                  return (
                    <tr key={`${field.key}_${field.name}`} className="border-b">
                      {defaultColumnsRender(field, operate, fields)}
                      <td hidden>
                        {hiddenFields?.map((hiddenFieldName, index) => {
                          return (
                            <Form.Item
                              wrapperCol={{ span: 24 }}
                              key={`${field.key}_${index}_${hiddenFieldName}`}
                              hidden
                              name={[field.name, hiddenFieldName]}
                              initialValue={null}
                            >
                              <Text />
                            </Form.Item>
                          );
                        }) ?? null}
                      </td>
                    </tr>
                  );
                })
              ) : (
                <tr>
                  <td colSpan={columns.length}>
                    <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                  </td>
                </tr>
              )}
              <tr>
                <td colSpan={columns.length}>
                  <Form.ErrorList errors={errors} />
                </td>
              </tr>
            </tbody>
          );
        }}
      </Form.List>
    </table>
  );
};

export default FormTable;

/** column配置了多级表头 */
function renderComplexColumn(columns: Column[]) {
  // 先收集有几层，目前支持2级数据
  const rowSpan = 2;
  // 渲染一级表头
  function renderParent() {
    return columns
      .filter((column) => !column.hidden)
      .map((column, index) => (
        <th
          key={`th_${column.dataIndex}_${index}`}
          rowSpan={column.children ? undefined : rowSpan}
          colSpan={column.children ? column.children.length : undefined}
          className={`${column.children ? 'leading-5 border-b ' : ''}${column.required ? 'required-mark ' : ''}`}
        >
          {column.title}
        </th>
      ));
  }
  // 渲染子级表头
  function renderChild() {
    const children: any[] = [];
    columns
      .filter((column) => !column.hidden && valueNotNullAndUndefined(column.children))
      .forEach((column) => {
        column.children?.forEach((childColumn, childIndex) => {
          children.push(
            <th key={`child_th_${childColumn.dataIndex}_${childIndex}`} className="leading-5">
              {childColumn.title}
            </th>,
          );
        });
      });
    return children;
  }
  return (
    <>
      <tr>{renderParent()}</tr>
      <tr>{renderChild()}</tr>
    </>
  );
}
