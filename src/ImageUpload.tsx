import React, { useCallback, useEffect, useState } from 'react';
import { CloudUploadOutlined, DeleteOutlined } from '@ant-design/icons';
import { Typography, Upload, UploadFile } from 'antd';
import dayjs from 'dayjs';
import { v4 as uuidv4 } from 'uuid';
import { fetcher } from './request';
import { valueNotNullAndUndefined } from './utils';
import SvgIcon from './SvgIcon';

/** oss前缀地址 */
const ossSrc = 'https://statics.wellyspring.com';
interface ImageUploadProps {
  maxCount: number;
  value?: string | UploadFile[];
  allowDelete?: boolean;
  /** 接受的文件类型默认 image/*,.pdf */
  accept?: string;
  /** 文件大小，默认10MB */
  size?: number;
  onChange?: (value: UploadFile[] | null) => void;
  onPreview?: (val: UploadFile) => void;
  onDelete?: (val: UploadFile) => void;
}

// 图片选择，传入value时，显示图片
const ImageUpload = function ImageUpload({
  value,
  maxCount,
  onChange,
  accept,
  size,
  allowDelete,
  onPreview,
  onDelete,
}: ImageUploadProps) {
  const [fileList, setFileList] = useState<UploadFile[]>([]);
  useEffect(() => {
    if (value) {
      if (typeof value === 'string') {
        // value是图片地址，直接转成需要的格式，然后重新触发更新
        onChange?.(
          value.split(',').map((url, index) => ({
            thumbUrl: url,
            url: value,
            name: getFileNameFromUrl(url) ?? 'file name',
            status: 'done',
            type: getFileSuffixFromUrl(url),
            uid: `upload-${new Date().valueOf().toString()}-${index}`,
          })),
        );
      } else {
        setFileList(value);
      }
    } else {
      // 如果没有value，则清空fileList
      setFileList([]);
    }
  }, [value]);
  /** 计算文件上传的提示信息 */
  const renderTip = useCallback(() => {
    const acceptFileTypesTip = accept!
      .split(',')
      .map((acceptStr) => {
        if (acceptStr.indexOf('image/*') !== -1) {
          // 支持所有的图片
          return '图片';
        } else {
          return acceptStr.replace('.', '');
        }
      })
      .join('、');
    return `最多可添加${maxCount}个附件，单个文件大小不超过${size}M，支持${acceptFileTypesTip}格式`;
  }, [maxCount, accept, size]);
  return (
    <>
      {fileList.length < maxCount ? (
        <Upload.Dragger
          className="animate-fade-in animate-ease-linear animate-duration-150"
          fileList={fileList}
          accept={accept}
          showUploadList={false}
          multiple={maxCount > 1}
          maxCount={maxCount}
          beforeUpload={() => false}
          onChange={(e: any) => {
            // 将选中的文件返回，总长度必须小于最大值
            const targetList =
              e?.fileList
                ?.filter((file: UploadFile) => {
                  if (valueNotNullAndUndefined(size)) {
                    return !file.size || file.size <= size! * 1024 * 1024;
                  } else {
                    return true;
                  }
                })
                ?.filter((file: UploadFile) => {
                  // accept只是引导用户选择正确的文件，用户仍然可以选择错误的文件，所以此处过滤
                  return isFileMatchAccept(file, accept!);
                })
                ?.map((file: UploadFile) => {
                  if (file.originFileObj) {
                    return {
                      ...file,
                      thumbUrl: file.originFileObj ? window.URL.createObjectURL(file.originFileObj) : null,
                      status: 'done',
                      type: getFileSuffixFromUrl(file.name),
                    };
                  } else {
                    return { ...file };
                  }
                }) ?? [];
            onChange?.(targetList.slice(0, maxCount));
          }}
        >
          <div className="h-93px w-full flex flex-col items-center justify-center">
            <p>
              <span className="text-18px mr-5px">
                <CloudUploadOutlined />
              </span>
              <span>上传文件或拖拽至此区域</span>
            </p>
            <p className="text-gray-400/45 leading-32px">{renderTip()}</p>
          </div>
        </Upload.Dragger>
      ) : null}
      {fileList?.map((file) => {
        return (
          <FileListItem
            file={file}
            key={file.uid}
            showDelete={allowDelete}
            onDelete={(file) => {
              const files = fileList.filter((item) => item.uid !== file.uid);
              setFileList(files);
              onChange?.(files);
            }}
          />
        );
      })}
    </>
  );
};
ImageUpload.defaultProps = {
  maxCount: 1,
  allowDelete: true,
  size: 10,
  accept: 'image/*,.pdf',
};

ImageUpload.uploadMultipleFilesToOss = uploadMultipleFilesToOss;
ImageUpload.getFileNameFromUrl = getFileNameFromUrl;
ImageUpload.getFileSuffixFromUrl = getFileSuffixFromUrl;
// 文件列表的item组件
ImageUpload.FileListItem = FileListItem;
export default ImageUpload;

/**
 * 文件上传列表Item
 */
function FileListItem({
  file,
  showDelete,
  onPreview,
  onDelete,
}: {
  file: UploadFile;
  showDelete?: boolean;
  onPreview?: (val: UploadFile) => void;
  onDelete?: (val: UploadFile) => void;
}) {
  return (
    <div
      key={file.uid}
      className="border border-gray-300 inline-flex h-50px w-240px items-center justify-between cursor-pointer relative mx-5px animate-fade-in animate-ease-linear animate-duration-150 mt-15px"
    >
      <div
        className="inline-flex w-full items-center justify-between px-5px"
        onClick={() => {
          onPreview?.(file);
          window.open(file.thumbUrl);
        }}
      >
        <span className="text-18px w-25px text-center">{<SvgIcon name={`icon-${file.type}`} />}</span>
        <Typography.Text ellipsis={{ tooltip: file.name }} className="text-black/65 flex-1">
          {file.name}
        </Typography.Text>
        {showDelete ? (
          <span
            className="text-black/45 hover:text-danger inline-block w-25px text-center"
            onClick={(e) => {
              e.preventDefault();
              e.stopPropagation();
              // delete
              onDelete?.(file);
            }}
          >
            <DeleteOutlined />
          </span>
        ) : null}
      </div>
    </div>
  );
}

/** 导出图片上传到oss方法 */
async function uploadFileToOSS(file: File, feedback?: string): Promise<string> {
  try {
    const ret = await fetcher<{ url: string }>(
      // 智采、智造文件上传的oss桶不一致，临时给智造专用接口
      `/server/getUploadUrl?key=/smartmes/files${feedback ?? ''}/${dayjs().format(
        'YYYY-MM-DD',
      )}/${uuidv4()}__${encodeURIComponent(file.name)}`,
    );
    // 上传到oss
    if (ret) {
      let uploadUrl = ret.url;
      // @ts-ignore
      if (import.meta.env.DEV) {
        // 开发环境，将上传前缀改为本地
        const length = uploadUrl.indexOf('.com') + 4;
        const replaceTargetStr = uploadUrl.substring(0, length);
        uploadUrl = uploadUrl.replaceAll(replaceTargetStr, '/oss');
      }
      await fetch(uploadUrl, { method: 'PUT', body: file });
      const fileSrc = ret.url.split('?')[0];
      return `${ossSrc}${fileSrc.split('.myqcloud.com').pop()}`;
    } else {
      return Promise.reject(new Error('获取上传地址失败'));
    }
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

/**
 * 将文件列表上传，并返回地址数组
 * @param files 待上传的文件列表
 */
function uploadMultipleFilesToOss(files: UploadFile[]): Promise<string[]> {
  return new Promise((resolve, reject) => {
    const queues: Promise<any>[] = [];
    files.forEach((file) => {
      if (file.originFileObj) {
        // 上传
        queues.push(uploadFileToOSS(file.originFileObj));
      } else {
        queues.push(Promise.resolve(file.thumbUrl));
      }
    });
    Promise.all(queues)
      .then((urls) => {
        resolve(urls);
      })
      .catch(reject);
  });
}

/**
 * 从地址中获取文件名和后缀
 * @param url 地址
 */
function getFileNameFromUrl(url: string) {
  const name = url.split('__').pop();
  return name ? decodeURIComponent(name) : name;
}

/**
 * 从地址或者文件名中获取后缀
 * @param url 文件地址
 * @returns 后缀
 */
function getFileSuffixFromUrl(url: string) {
  const suffix = url
    .match(
      /\.(jpg|jpeg|png|bmp|pdf|svg|doc|docx|dps|dpt|csv|flv|gif|mp4|ppt|pptx|tif|tiff|txt|word|wps|wpt|wt|xls|xlsx|xml){1}/gi,
    )
    ?.pop();
  if (valueNotNullAndUndefined(suffix)) {
    return suffix!.replaceAll('.', '');
  } else {
    return 'other';
  }
}

/**
 * 判断给定的文件是否符合accept的要求；判断文件类型和文件后缀
 * @param file
 * @param accept
 */
function isFileMatchAccept(file: UploadFile, accept: string) {
  if (accept === '*') {
    // 允许所有文件
    return true;
  }
  if (accept.indexOf('image/*') !== -1) {
    // accept 允许了所有图片，判断文件类型如果是图片就直接允许，否则走通配逻辑
    if (file.originFileObj?.type.indexOf('image/') !== -1) {
      return true;
    }
  }
  // 没有制定通配符，则文件后缀必须强校验；如果没有文件名，则认为不符合要求
  return (
    valueNotNullAndUndefined(file?.originFileObj?.name) &&
    accept.indexOf(getFileSuffixFromUrl(file?.originFileObj?.name!)) !== -1
  );
}
