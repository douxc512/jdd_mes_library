/**
 * 移除字符串的空格
 */
export function trim(v: string) {
  return v.replace(/\s+/g, '');
}
/** 判断值不为null且不为undefined */
export function valueNotNullAndUndefined(v: Record<string, any> | string | number | boolean | null | undefined) {
  return v !== null && v !== undefined;
}

/** 递归将树形字典转为一层结构 */
export function recursionSpreadDictionary<T extends Array<any>>(sourceData: T): Record<string, any> {
  let sourceValue: Record<string, any> = {};
  sourceData.forEach((record) => {
    sourceValue[record.dictValue] = record.dictName;
    if (record.children && record.children.length > 0) {
      const ret = recursionSpreadDictionary<T>(record.children);
      sourceValue = { ...sourceValue, ...ret };
    }
  });
  return sourceValue;
}
