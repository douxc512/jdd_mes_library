import React from 'react';
import { Space, InputNumber } from 'antd';

/** 规格型号搜索条件 */
interface SpecInput {
  value?: { length: number | undefined; width: number | undefined; height: number | undefined };
  layout: ('length' | 'width' | 'height')[];
  onChange?: (v: SpecInput['value']) => void;
}
function SpecInput({ value, layout, onChange }: SpecInput) {
  return (
    <Space>
      {layout.indexOf('length') !== -1 ? (
        <InputNumber
          value={value?.length ?? undefined}
          className="w-72px"
          controls={false}
          placeholder="长"
          onChange={(v) => {
            onChange?.({ length: v ?? undefined, width: value?.width, height: value?.height });
          }}
        />
      ) : null}
      {layout.indexOf('width') !== -1 ? (
        <>
          <span>*</span>
          <InputNumber
            value={value?.width ?? undefined}
            className="w-72px"
            controls={false}
            placeholder="宽"
            onChange={(v) => {
              onChange?.({ width: v ?? undefined, length: value?.length, height: value?.height });
            }}
          />
        </>
      ) : null}
      {layout.indexOf('height') !== -1 ? (
        <>
          <span>*</span>
          <InputNumber
            value={value?.height ?? undefined}
            className="w-72px"
            controls={false}
            placeholder="高"
            onChange={(v) => {
              onChange?.({ height: v ?? undefined, width: value?.width, length: value?.length });
            }}
          />
        </>
      ) : null}
    </Space>
  );
}
SpecInput.defaultProps = {
  layout: ['length', 'width', 'height'],
};
export default SpecInput;
