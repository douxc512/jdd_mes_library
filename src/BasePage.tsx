import React, {
  cloneElement,
  Dispatch,
  isValidElement,
  ReactElement,
  ReactNode,
  SetStateAction,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { UpOutlined } from '@ant-design/icons';
import { Button, Card, Col, Form, FormInstance, Row, Space, Tabs } from 'antd';
import { useGet } from './request';
import { valueNotNullAndUndefined } from './utils';

/** 搜索条件中需要处理的日期key，可以指定处理的key的值 */
type DateKeyProp = (string | Record<string, string[]>)[];
/** tabs切换标签页的value */
type Status = number | string;
/** 渲染表格传入的参数 */
type TableRenderProps = {
  dataSource: any[];
  setDataSource: Dispatch<SetStateAction<any[]>>;
  status?: Status;
  originData: { data: any[] | null; count: number; sum?: number; [key: string]: any } | null;
  summaryData?: Record<string, number> | null;
};

/** 搜索条件项 */
interface SearchItem {
  label: string;
  name: string | string[];
  valuePropName?: string;
  initialValue?: any;
  element: ReactNode | ((v: Record<string, any>) => ReactNode);
}
/** 组件的属性 */
interface Props {
  /** 数据请求地址 */
  queryUrl: string;
  /** 导出地址 */
  exportUrl?: string;
  /** 统计数据地址 */
  summaryUrl?: string;
  /** 自定义获取dataSource的方法，因为当前返回的数据中可能从list、data、本身直接获取数据 */
  getDataSource: (data: any) => any;
  /** 字段取值调整 */
  dataSourceName?: string;
  totalCountName?: string;
  /** table渲染方法 */
  tableRender: (args: TableRenderProps) => ReactElement;
  /** 操作按钮 */
  operates?: (status: Status) => ReactNode;
  /** tabs */
  tabs?: {
    defaultCheck?: Status;
    options?: { label: string; value: Status }[];
    onChange?: (v: Status, searchFormInstance?: FormInstance) => void;
    external?: (v: { status: Status; data: { count: number; sum?: number; [key: string]: any } | null }) => ReactNode;
  };
  /** 顶部搜索条件 */
  search?: SearchItem[];
  /**
   * 对search的过滤
   * 有场景需要根据选择的tab动态渲染顶部search
   * v={status}
   */
  searchFilter?: (search: SearchItem, v: Record<string, any>) => boolean;
  searchValueFormat: (v: Record<string, any>, options: { status?: Status }) => Record<string, any>;
  /** 页面其他元素 */
  children?: ReactNode | ReactNode[];
  /**
   * 需要格式化处理的日期搜索key
   */
  dateKeys: DateKeyProp;
}

const BasePageEvent = new Map();

const BasePage = function ({
  queryUrl,
  summaryUrl,
  getDataSource,
  search,
  searchFilter,
  searchValueFormat,
  tableRender,
  operates,
  tabs,
  exportUrl,
  dateKeys,
  children,
}: Props) {
  // 列表数据
  const [page, setPage] = useState(1);
  const [size, setSize] = useState(10);
  const [status, setStatus] = useState<Status | undefined>(tabs?.defaultCheck ?? undefined);
  // 搜索条件
  const [searchData, setSearchData] = useState<Record<string, any> | null>(null);

  const {
    loading: queryLoading,
    data: listData,
    request: queryRequest,
  } = useGet<{ data: any[] | null; count: number; sum?: number; list?: any[]; totalCount?: number }>(queryUrl, {
    manual: true,
  });
  // 统计数据
  const { data: summaryData, request: querySummary } = useGet<Record<string, number>>(summaryUrl, { manual: true });
  function refreshList(v: Record<string, any>) {
    const values = searchDateFormat(v, dateKeys);
    const data = searchValueFormat(values, { status });
    queryRequest(data);
    if (valueNotNullAndUndefined(summaryUrl)) {
      querySummary(data);
    }
  }
  // 表格数据
  const [dataSource, setDataSource] = useState<any[]>([]);
  useEffect(() => {
    setDataSource(getDataSource(listData)?.map((item: any) => ({ ...item, showAll: false })) ?? []);
  }, [listData]);
  useEffect(() => {
    form.validateFields().then((values) => {
      refreshList({ page: 1, size: 10, status, ...values });
    });
  }, []);

  // 复制一份table
  const tableRef = useRef<any>();
  const table = useMemo(() => {
    const tableEle = tableRender({ dataSource, setDataSource, status, originData: listData, summaryData });
    // 添加默认的分页处理
    const clonedEle = cloneElement(tableEle, {
      ref: tableRef,
      loading: queryLoading,
      pagination:
        tableEle.props.pagination === null
          ? null
          : {
              current: page,
              pageSize: size,
              total: listData?.count || listData?.totalCount,
              onChange: (page: number, size: number) => {
                setPage(page);
                setSize(size);
                refreshList({ ...searchData, status, page, size });
              },
            },
    });
    return clonedEle;
  }, [dataSource, listData, setDataSource, status, tableRender, queryLoading, summaryData, searchData]);

  // 导出
  const { request: exportRequest } = useGet(exportUrl, {
    manual: true,
  });
  function exportHandle(v: Record<string, any>) {
    const value = searchDateFormat(v, dateKeys);
    const data = searchValueFormat(value, { status });
    exportRequest(data);
  }
  // tabs渲染
  const tabsExternalRender = useCallback(() => {
    return tabs?.external?.({ status: status!, data: listData });
  }, [status, listData, tabs?.external]);

  /**
   * 刷新列表，分页重置、顶部搜索条件不做处理
   **/
  const refresh = useCallback(
    async (params?: any) => {
      setPage(1);
      setSize(10);
      // const values = await form.validateFields();
      // 清空已选的key
      tableRef?.current?.clearRowSelection?.();
      refreshList({ status, page: 1, size: 10, ...searchValueFormat(searchData ?? {}, { status }), ...params });
    },
    [status, tableRef.current, searchData],
  );
  // 注册本地处理
  useEffect(() => {
    BasePageEvent.set('refresh', refresh);
    BasePageEvent.set('export', async () => {
      await exportHandle({ ...searchData, status });
    });
    return function () {
      BasePageEvent.clear();
    };
  }, [status]);
  // 搜索条件
  const [form] = Form.useForm();
  const [collapse, setCollapse] = useState(false);
  const [showCollapseBtn, setShowCollapseBtn] = useState(false);
  useEffect(() => {
    if (search && search?.length > 2) {
      setShowCollapseBtn(true);
    }
  }, [search]);

  return (
    <>
      {search ? (
        <div className="p-20px bg-white pb-4px">
          <Form
            form={form}
            layout="horizontal"
            labelCol={{ span: 6 }}
            onFinish={(values) => {
              // 点击顶部查询，应该重置分页、使用搜索条件
              setSearchData(values);
              setPage(1);
              setSize(10);
              refreshList({ status, page: 1, size: 10, ...searchValueFormat(values ?? {}, { status }) });
            }}
            onReset={() => {
              // 重置了，重置分页数据，保留状态
              // 表单搜索条件支持默认值，所以此处通过refresh刷新
              // refresh刷新时会取当前的form数据
              setSearchData(null);
              setPage(1);
              setSize(10);
              refreshList({ status, page: 1, size: 10, ...searchValueFormat({}, { status }) });
            }}
          >
            <Row gutter={16} justify="space-between">
              {search
                ?.filter((item) => {
                  if (valueNotNullAndUndefined(searchFilter)) {
                    return searchFilter!(item, { status });
                  } else {
                    return true;
                  }
                })
                .filter((item, index) => {
                  // 需要处理展开、收起
                  if (showCollapseBtn) {
                    if (!collapse && index > 1) {
                      return false;
                    } else {
                      return true;
                    }
                  } else {
                    return true;
                  }
                })
                .map((item) => (
                  <Col xxl={{ span: 6 }} span={8} key={item.name.toString()}>
                    <Form.Item
                      initialValue={item.initialValue ?? undefined}
                      valuePropName={item.valuePropName ?? 'value'}
                      label={item.label}
                      name={item.name}
                      className="mb-16px"
                    >
                      {isValidElement(item.element) ? item.element : (item.element as any)?.({ status })}
                    </Form.Item>
                  </Col>
                ))}
              <Col xxl={{ span: 6 }} span={8} style={{ marginLeft: 'auto', textAlign: 'right' }}>
                <Space>
                  {showCollapseBtn ? (
                    <span
                      className="cursor-pointer text-primary"
                      onClick={() => {
                        setCollapse((prev) => !prev);
                      }}
                    >
                      <span className="mr-5px">{`${collapse ? '收起' : '展开'}`}</span>
                      <UpOutlined
                        className="transition-all"
                        style={{
                          fontSize: 12,
                          transform: `rotate(${collapse ? '0' : '180deg'})`,
                        }}
                      />
                    </span>
                  ) : null}
                  <Button type="primary" htmlType="submit">
                    查询
                  </Button>
                  <Button htmlType="reset">重置</Button>
                </Space>
              </Col>
            </Row>
          </Form>
        </div>
      ) : null}
      <Card style={{ marginTop: 16 }} bodyStyle={{ padding: 16 }}>
        {operates?.(status!)}
        {tabs ? (
          <Tabs
            className="status-radio w-full"
            // 此处value类型强制为string，实际使用会转回number
            defaultActiveKey={tabs.defaultCheck?.toString()}
            onChange={(val) => {
              setStatus(val === '' ? val : (Number(val) as Status));
              tabs.onChange?.(val === '' ? val : (Number(val) as Status), form);
              // tab切换，分页应该重置，搜索条件不变
              refresh({ ...searchData, status: val });
            }}
            tabBarExtraContent={tabsExternalRender()}
            items={tabs.options?.map((op) => ({ ...op, key: `${op.value}` }))}
          />
        ) : null}
        {table}
        {children}
      </Card>
    </>
  );
};
BasePage.defaultProps = {
  dataSourceName: 'data',
  totalCountName: 'count',
  searchValueFormat: (v: Record<string, any>) => v,
  dateKeys: ['deliveryDate', 'deliveryTime', 'saleDate', 'purchaseDate', 'createTime', 'createDate', 'paymentDate'],
  /** 默认返回的列表数据可以从list、data中获取到 */
  getDataSource: (v: any) => v?.list || v?.data || [],
};
/** 重置搜索条件 */
BasePage.reset = function () {
  console.warn('BasePage.reset 已经移除，使用BasePage.refresh');
};
/**
 * 刷新列表
 */
BasePage.refresh = function () {
  BasePageEvent.get('refresh')?.();
};
/**
 * 触发导出
 */
BasePage.export = async function () {
  await BasePageEvent.get('export')?.();
};
export default BasePage;

/** 移除空字符串、null、undefined值 */
function removeUnValidValue(values: Record<string, any>) {
  const result = { ...values };
  Object.keys(result).forEach((key) => {
    if (result[key] === '' || result[key] === null || result[key] === undefined) {
      delete result[key];
    }
  });
  return result;
}
/**
 * 从给定数据中格式化标记的key
 * @param values 传入的数据
 * @param dateKeys 需要格式化操作的key
 * @returns
 */
function searchDateFormat(values: Record<string, any>, dateKeys: DateKeyProp) {
  const formattedValue = { ...values };
  dateKeys.forEach((key) => {
    if (typeof key === 'string') {
      if (valueNotNullAndUndefined(values[key])) {
        // 需要处理
        formattedValue[`${key}Start`] = values[key][0].format('YYYY-MM-DD 00:00:00');
        formattedValue[`${key}End`] = values[key][1].format('YYYY-MM-DD 23:59:59');
        delete formattedValue[key];
      }
    } else {
      // 自定义了处理key的获取方式
      const objKey = Object.keys(key)?.[0];
      const [start, end] = key[objKey];
      if (valueNotNullAndUndefined(values[objKey])) {
        // 需要处理
        formattedValue[`${start}`] = values[objKey][0].format('YYYY-MM-DD 00:00:00');
        formattedValue[`${end}`] = values[objKey][1].format('YYYY-MM-DD 23:59:59');
        delete formattedValue[objKey];
      }
    }
  });
  return removeUnValidValue(formattedValue);
}
