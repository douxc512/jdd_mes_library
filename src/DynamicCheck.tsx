// 选择了checkbox时，才进一步显示后续内容
// check - checkbox
// value - 后续内容绑定的值
// extend - 扩展内容的值
// 目前仅用于生产工序使用
import { Checkbox, Space } from 'antd';
import React, { cloneElement, ReactElement, useCallback, useEffect, useState } from 'react';

interface Props {
  label?: string;
  /** 完整的process对象 */
  value?: Record<string, number | string | null>;
  valuePropName: string;
  children: ReactElement | null;
  onChange?: (v: Props['value']) => void;
  /** 扩展字段 */
  extendPropName: string;
  requiredMark: boolean;
}
const DynamicCheck = function ({
  label,
  value,
  valuePropName,
  children,
  onChange,
  extendPropName,
  requiredMark,
}: Props) {
  const [checked, setChecked] = useState<boolean>(false);
  const [data, setData] = useState<string | number | null>(null);
  const [extendData, setExtendData] = useState<string | number | null>(null);
  const [child, setChild] = useState<ReactElement | null>(null);

  /** 传入的子组件的onChange事件 */
  const emitChange = useCallback(
    (e: any, check?: boolean) => {
      const retValue: Record<string, string | number | null> = { ...value, isChecked: Number(check) };
      if (typeof e === 'boolean') {
        // checkbox变化引起
        retValue[valuePropName] = e ? data : null;
        retValue[extendPropName] = e ? extendData : null;
        retValue.isChecked = Number(e);
      } else if (typeof e === 'object') {
        retValue[valuePropName] = e[valuePropName];
        retValue[extendPropName] = e[extendPropName];
      }
      onChange?.(retValue);
    },
    [value, data, extendData],
  );

  useEffect(() => {
    if (children) {
      const ret: any = {};
      ret[valuePropName] = data;
      ret[extendPropName] = extendData;
      const ele = cloneElement(children, {
        value: ret,
        onChange(e: any) {
          // 调用组件本身的change事件监听
          children?.props?.onChange?.(e);
          emitChange(e, true);
        },
      });
      setChild(ele);
    } else {
      setChild(null);
    }
  }, [children, data, extendData]);
  useEffect(() => {
    if (value) {
      // 传入了值，区分是否选中，如果没有选中，则data为null
      const isChecked = Boolean(value.isChecked);
      setChecked(isChecked);
      setData(value?.[valuePropName]);
      setExtendData(value?.[extendPropName]);
    } else {
      // 默认值
      setChecked(false);
      setData(null);
      setExtendData(null);
    }
  }, [value]);
  return (
    <Space>
      {checked && requiredMark && <span className="required-mark" />}
      {label}
      <Checkbox
        checked={checked}
        onChange={(e) => {
          const isChecked = e.target.checked;
          setChecked(isChecked);
          emitChange(isChecked);
        }}
      />
      {checked ? child : null}
    </Space>
  );
};
DynamicCheck.defaultProps = {
  valuePropName: 'value',
  requiredMark: true,
};
export default DynamicCheck;
