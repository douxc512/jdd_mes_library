import { valueNotNullAndUndefined } from './utils';
import { Typography, Empty, Image } from 'antd';
import React, { cloneElement, FC, ReactElement } from 'react';

// 图片预览，200*200+底部name
interface Props {
  size?: number;
  src?: string | null;
  title: string;
  image?: ReactElement;
}
/**
 * 图片示例图展示，默认大小200px
 */
const SampleGraph: FC<Props> = function ({ size, title, src, image }) {
  function renderImage() {
    // 如果src没有值，则直接展示Empty
    if (!valueNotNullAndUndefined(src) || src === '') {
      return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="" />;
    }
    // 如果自定义了image，就将src属性赋值到传入的image上
    if (valueNotNullAndUndefined(image)) {
      return cloneElement(image!, { src, alt: title });
    }
    // 没有自定义，则默认使用Image
    return <Image src={src!} alt={title} />;
  }
  return (
    <div className="inline-flex flex-col items-center" style={{ width: `${size}px` }}>
      <div
        className="inline-flex items-center justify-center overflow-hidden"
        style={{ height: `${size}px`, width: `${size}px` }}
      >
        {renderImage()}
      </div>
      <Typography.Text className="text-black/45" ellipsis={{ tooltip: title }}>
        {title}
      </Typography.Text>
    </div>
  );
};
SampleGraph.defaultProps = {
  size: 200,
};

export default SampleGraph;
