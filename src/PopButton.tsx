import React, { ReactNode, useState } from 'react';
import { Popconfirm, Button, ButtonProps, PopconfirmProps } from 'antd';
import SvgIcon from './SvgIcon';

interface Props extends PopconfirmProps, Omit<ButtonProps, 'title' | 'color'> {
  type?: 'link' | 'primary';
  onConfirm?: () => void;
  onBeforeVisible: () => boolean;
  buttonText: string;
  okText: string;
  okIcon?: ReactNode;
}

/** 需要进一步确认的按钮，带popConfirm */
const PopButton = ({
  onConfirm,
  loading,
  disabled,
  onBeforeVisible,
  title,
  buttonText,
  okText,
  type,
  okButtonProps,
  okIcon,
}: Props) => {
  const [visible, setVisible] = useState(false);
  return (
    <Popconfirm
      title={title}
      open={visible}
      disabled={disabled}
      onOpenChange={(v) => {
        if (!v) {
          setVisible(false);
          return;
        }
        setVisible(onBeforeVisible());
      }}
      onConfirm={onConfirm}
      okText={okText}
      okButtonProps={{ loading, ...okButtonProps }}
    >
      <Button
        disabled={disabled}
        loading={loading}
        type={type ?? 'link'}
        className={type ? '' : 'primary-link-btn'}
        icon={okIcon ?? <SvgIcon name="icon-cancel" />}
      >
        {buttonText}
      </Button>
    </Popconfirm>
  );
};
PopButton.defaultProps = {
  title: '确认取消已选订单吗？',
  buttonText: '取消',
  okText: '确认取消',
  onBeforeVisible() {
    return true;
  },
  disabled: false,
  okButtonProps: { type: 'primary', danger: true },
};
export default PopButton;
