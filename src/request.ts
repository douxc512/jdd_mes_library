import React, { useEffect, useState } from 'react';
import { message } from 'antd';

const headers = {
  'content-type': 'application/json; charset=utf-8;',
};
/** 从服务器返回的数据结构 */
interface ServerResponse<T> {
  code: number;
  data: T;
  message: string | null;
  /** 接口返回错误的时候有status没有code */
  status?: number;
  /** sso 平台返回会有result */
  result?: number;
}

/** 重新登录 */
function reLogin() {
  // 没有token
  setTimeout(() => {
    // 重新登录
    window.location.replace(`/login?redirect=${encodeURIComponent(window.location.href)}`);
  }, 1000);
}

/** 请求成功之后根据业务code处理 */
function parseResponse<T>(response: ServerResponse<T>) {
  if (response.code === 200) {
    return Promise.resolve(response.data);
  } else if (response.code === 82024 || response.code === 82023 || response.result === 401) {
    // sso 认证平台登录过期
    message.error(response.message);
    reLogin();
    return;
  } else {
    // 默认提示错误信息
    message.error(response.message || `业务接口错误,Status:${response.status}`);
    return Promise.reject(response);
  }
}
// eslint-disable-next-line no-undef
interface FetcherInitialOptions extends RequestInit {
  params?: Record<string, any>;
  onResponse?: (v: any) => void;
}
/** 默认的请求函数，对原生的fetch添加了处理 */
export const fetcher = <T = null>(url: string, init: FetcherInitialOptions = { method: 'GET' }) => {
  // 默认取配置的headers，如果init传入，则以传入的为准
  const targetHeaders = new Headers({ ...init.headers });
  // add token to headers
  if (localStorage.getItem('token')) {
    targetHeaders.set('authorization', `Bearer ${localStorage.getItem('token')}`);
  }
  // 如果是post请求，自动加上header
  let fetchUrl = url;
  if (init.method === 'POST' && typeof init.body === 'string') {
    targetHeaders.append('content-type', headers['content-type']);
  } else if (!init?.method || init.method === 'GET') {
    // get 请求，将params自动转为urlParams
    const urlParams = new URLSearchParams();
    if (init.params) {
      Object.keys(init.params).forEach((key) => {
        const value = init.params![key];
        if (value !== null && value !== undefined) {
          urlParams.append(key, value);
        }
      });
    }
    if (urlParams.toString() !== '') {
      fetchUrl = url.indexOf('?') !== -1 ? `${url}&${urlParams.toString()}` : `${url}?${urlParams.toString()}`;
    }
    delete init.params;
  }
  return fetch(fetchUrl, { ...init, headers: targetHeaders, credentials: 'include' })
    .then((r) => {
      // 请求成功，针对网络请求的状态码进行处理
      const { status } = r;
      if (status === 401) {
        message.error('登录已过期，请重新登录');
        reLogin();
        return Promise.reject(new Error('登录过期'));
      }
      if (status === 404) {
        message.error(`接口返回404,url: ${url}`);
        return Promise.reject(new Error(`接口返回404,url: ${url}`));
      }
      return r.clone();
    })
    .then(async (ret: Response) => {
      if (ret.headers.get('content-disposition')) {
        // attachment;filename=%E5%AF%B9%E8%B4%A6%E5%8D%95.xlsx;filename*=utf-8''%E5%AF%B9%E8%B4%A6%E5%8D%95.xlsx
        const fileBlob = await ret.blob();
        const a = document.createElement('a');
        const link = window.URL.createObjectURL(fileBlob);
        const filenameStr = ret.headers.get('content-disposition')?.split(';')[1] as string;
        const filename = decodeURIComponent(filenameStr).replace(new RegExp('filename=', 'g'), '');
        if (filename.endsWith('html')) {
          // 打印单据时的html
          return link as any as T;
        }
        a.href = link;
        a.download = filename || 'file.xlsx';
        a.click();
        return;
      }
      init.onResponse?.(ret);
      // 解析成功，针对自定义code处理
      try {
        return parseResponse((await ret.json()) as ServerResponse<T>);
      } catch (error) {
        console.error(error);
      }
    })
    .catch((err) => {
      return Promise.reject(err);
    });
};
type RequestFunc<T = any> = ({ ...restProps }?) => Promise<T>;
/**
 * post请求处理，比fetcher多了loading；默认不会触发请求，需要手动调用request
 * get请求可以使用useGet处理
 * */
export function usePost<T = any>(url: string | RequestFunc) {
  const [loading, setLoading] = useState<boolean>(false);
  const [response, setResponse] = useState<T | null>(null);
  const [error, setError] = useState<any>(null);

  async function request(data: any) {
    try {
      setLoading(true);
      if (typeof url === 'string') {
        const ret = await fetcher<T>(url, {
          method: 'POST',
          body: JSON.stringify(data),
        });
        setResponse(ret as T);
        setError(null);
        return ret as T;
      } else {
        // 传入的函数
        const ret = await url?.(data);
        setResponse(ret as T);
        setError(null);
        return ret as T;
      }
    } catch (err: any) {
      setError(err);
      return Promise.reject(err);
    } finally {
      setLoading(false);
    }
  }
  return { loading, data: response, error, request };
}

/**
 * swr的缓存策略不太适合pc端toB的场景，尝试自己处理一个
 * 根据url和传入的依赖发起请求，依赖改变就重新触发
 * 也可以根据返回的request，手动发起请求
 * 可以传入配置{manual:boolean}控制默认的时候是否自动发起请求
 * 支持传入params，自动添加到url
 */
export function useGet<T = any>(url: string | undefined, option?: { manual?: boolean; params?: Record<string, any> }) {
  const [loading, setLoading] = useState(false);
  const [response, setResponse] = useState<T | null>(null);
  const [error, setError] = useState<any>(null);
  async function request(fetchUrl?: string | Record<string, any>, params?: Record<string, any>) {
    try {
      setLoading(true);
      let requestUrl = url;
      let sendParams = { ...option?.params, ...params };
      if (typeof fetchUrl === 'string') {
        requestUrl = fetchUrl;
      } else {
        // 传入的参数
        sendParams = { ...sendParams, ...fetchUrl };
      }
      // 处理params
      const urlParams = new URLSearchParams();
      Object.keys(sendParams)
        .filter((key) => {
          return sendParams[key] !== null && sendParams[key] !== undefined;
        })
        .forEach((key) => {
          urlParams.append(key, sendParams[key]);
        });
      const hasQuestionMark = requestUrl?.indexOf('?') !== -1;
      const ret = await fetcher<T>(`${requestUrl}${hasQuestionMark ? '&' : '?'}${urlParams.toString()}`);
      setResponse(ret as T);
      setError(null);
      return ret;
    } catch (err: any) {
      setError(err);
      return Promise.reject(err);
    } finally {
      setLoading(false);
    }
  }
  useEffect(() => {
    // url存在，且是非手动触发
    if (url && !option?.manual) {
      request(url, option?.params);
    }
  }, [url]);
  return { loading, data: response, error, request };
}

/** 配合service使用 */
export function useRequest<T = any>(
  reqFunc: RequestFunc,
  options?: {
    manual?: boolean;
    params?: Record<string, any>;
  },
) {
  const [loading, setLoading] = useState(false);
  const [response, setResponse] = useState<T | null>(null);
  const [error, setError] = useState<any>(null);
  async function request(params?: any) {
    try {
      setLoading(true);
      let ret;
      if (Array.isArray(params)) {
        ret = await reqFunc(params);
      } else {
        ret = await reqFunc({ ...options?.params, ...params });
      }
      setResponse(ret);
      return ret;
    } catch (error) {
      console.error(error);
      setError(error);
    } finally {
      setLoading(false);
    }
  }
  useEffect(() => {
    if (!options?.manual) {
      request();
    }
  }, [options]);
  return { loading, data: response, error, request };
}
