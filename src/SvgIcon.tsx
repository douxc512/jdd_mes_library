import React from 'react';
export default function SvgIcon({ name, active }: { name: string; active?: boolean }) {
  return (
    <svg className="icon inline-block mr-5px" aria-hidden="true">
      <use xlinkHref={`#${name}${active ? '-checked' : ''}`} />
    </svg>
  );
}
