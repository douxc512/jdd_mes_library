import React, { useEffect, useState } from 'react';
import { Button, ButtonProps } from 'antd';

interface Props extends ButtonProps {
  delay: number;
}
/**
 * 对ant-design 的Button组件二次封装，自动处理loading状态
 *
 * onClick支持异步任务，当异步任务在配置时间(默认为0，即立即进入loading状态)内没有结束时，button进入loading状态
 * 异步任务执行结束，自动取消loading状态
 */
const LoadingButton = function ({ children, onClick, delay, loading, ...restProps }: Props) {
  const [isLoading, setIsLoading] = useState(false);
  let timeInstance: number | undefined;

  const handleClick: React.MouseEventHandler<HTMLElement> = async (e) => {
    try {
      if (delay === 0) {
        setIsLoading(true);
      } else {
        timeInstance = setTimeout(() => {
          setIsLoading(true);
        }, delay);
      }
      await onClick?.(e);
    } catch (error) {
      console.error(error);
    } finally {
      clearTimeout(timeInstance);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    return function () {
      clearTimeout(timeInstance);
      setIsLoading(false);
    };
  }, []);

  return (
    <Button {...restProps} loading={isLoading || loading} onClick={handleClick}>
      {children}
    </Button>
  );
};
LoadingButton.defaultProps = {
  delay: 0,
};
export default LoadingButton;
