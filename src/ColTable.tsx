// 因为列表返回的格式类似于{a:xx,b:[{c:xx,d:xx},{c:xx,d:xx}]}
// ant-design的table不能直接渲染这种格式的数据，如果使用colSpan/rowSpan属性，会导致分页数据计算错误
// 所以需要针对数组部分单独处理渲染
// 目前涉及到的都是产品列表，所以暂时不做通用版本
import React from 'react';
import { Typography } from 'antd';
import { ReactNode } from 'react';

/**
 * 循环渲染表格数据，支持自定义格式化
 */
export function renderSubCellTDs(
  record: { productList?: any[]; list?: any[]; showAll: boolean; key?: string },
  name: string,
  format?: (item: any, source: any) => ReactNode,
) {
  // 最终取值的格式化函数，默认直接展示取出的值
  // 表格列宽根据属性指定，超出的展示...
  const valFormat =
    format ||
    ((val: string | number) => <Typography.Text ellipsis={{ tooltip: val }}>{val?.toString() || '-'}</Typography.Text>);
  // 兼容之前产品的渲染方式
  const key = record.key || 'productNo';
  let list = record.productList || record.list;
  if (!record.showAll) {
    // 默认取前3个值展示
    list = list?.slice(0, 3);
  }
  return list?.map((item, index) => {
    return (
      <div className="table-cell-list-item" key={`${item[key]}_${index}`}>
        {valFormat(item[name], item)}
      </div>
    );
  });
}
/** 根据给定的数组和记录，改变记录上的showAll字段，返回修改后的数组 */
export function setRecordAsShowAll(originRecords: any[], compareFun: (a: any) => boolean) {
  const newDataSource = [...originRecords];
  const index = newDataSource.findIndex((item) => compareFun(item));
  newDataSource[index].showAll = !newDataSource[index].showAll;
  return newDataSource;
}
