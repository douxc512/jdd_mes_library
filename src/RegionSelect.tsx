// 省市区三级联动查询

import React, { useState, useEffect } from 'react';
import { valueNotNullAndUndefined } from './utils';
import { fetcher } from './request';
import { Cascader } from 'antd';

interface DefaultOptionType {
  disabled?: boolean;
  label: React.ReactNode;
  value?: string | number | null;
  children?: Omit<DefaultOptionType, 'children'>[];
  [name: string]: any;
}

interface Region {
  areaname: string;
  id: number;
  lat: string;
  /** 共4级，到街道 */
  level: 1 | 2 | 3 | 4;
  lng: string;
  parentid: number;
  position: string;
  shortname: string;
  sort: number;
}

interface Props {
  value?: { province: string; city: string; county: string } | string;
  onChange?: (val: { province: string; city: string; county: string } | undefined) => void;
}
// 最后一级level
const LAST_LEVEL = 3;
// 省市区下拉选择
export default function RegionSelect({ value, onChange }: Props) {
  const [address, setAddress] = useState<DefaultOptionType[]>([]);
  // 获取省市区数据
  async function fetchData(op: DefaultOptionType) {
    const region = await fetcher<{ data: Region[] }>('/v1/mesArea/query', {
      headers: { 'Content-Type': 'application/json; charset=utf-8;' },
      method: 'POST',
      body: JSON.stringify({
        page: 1,
        size: 100,
        criteria: { parentid: op.value, level: op.level + 1 },
      }),
    });
    return region;
  }
  // 远程加载数据
  const loadRegion = async function (selectedOptions: DefaultOptionType[]) {
    const targetOption = selectedOptions[selectedOptions.length - 1];
    targetOption.loading = true;
    const region = await fetchData(targetOption);
    targetOption.children =
      region?.data.map((item) => ({
        label: item.shortname,
        value: item.id,
        isLeaf: item.level === LAST_LEVEL,
        level: item.level,
      })) ?? [];
    targetOption.loading = false;
    setAddress([...address]);
  };
  const [loading, setLoading] = useState(false);
  // 默认获取第一级数据
  useEffect(() => {
    (async () => {
      try {
        setLoading(true);
        const ret = await fetchData({ value: 1, level: 0, label: '顶级' });
        setAddress(
          ret?.data.map((item) => ({
            label: item.areaname,
            value: item.id,
            isLeaf: item.level === LAST_LEVEL,
            level: item.level,
          })) ?? [],
        );
      } catch (error) {
        console.error(error);
      } finally {
        setLoading(false);
      }
    })();
  }, []);
  return (
    <Cascader
      loading={loading}
      value={
        typeof value === 'string'
          ? value.split('/')
          : value?.province
          ? [value.province, value.city, value.county]
          : undefined
      }
      loadData={loadRegion}
      options={address}
      onChange={(val, selectedOptions) => {
        if (valueNotNullAndUndefined(selectedOptions)) {
          const [province, city, county] = selectedOptions;
          onChange?.({
            province: province.label as string,
            city: city.label as string,
            county: county.label as string,
          });
        } else {
          onChange?.(undefined);
        }
      }}
    />
  );
}
