// 金额格式化
import React from 'react';
import { valueNotNullAndUndefined } from './utils';
import { InputNumber, InputNumberProps, Tooltip } from 'antd';

interface CurrencyInputProps extends InputNumberProps {
  hightLight?: boolean;
}
/**
 * 将给定的金额格式化
 * @param value 金额
 * @returns
 */
function formatter(value: string | number | null | undefined) {
  // .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  return valueNotNullAndUndefined(value) ? `¥ ${value}` : value;
}
/**
 * 将格式化之后的金额数据转回数值
 * @param value 格式化之后的数据
 * @returns
 */
function parser(value: string | null | undefined) {
  if (valueNotNullAndUndefined(value)) {
    const valueStr = value!.replace(/\¥\s?|(,*)/g, '');
    return valueStr.length > 0 ? Number(valueStr) : value;
  } else {
    return value;
  }
}
/**
 * 格式化金额之后展示
 * @param param0 需要格式化的金额
 * @returns
 */
const Currency = function ({ value, className }: CurrencyInputProps) {
  if (valueNotNullAndUndefined(value)) {
    const val = Number(value).toFixed(3);
    const formatValue = formatter(Number(val));
    return (
      <Tooltip title={formatter(value)}>
        <span className={`currency ${className ?? ''}`}>{formatValue}</span>
      </Tooltip>
    );
  } else {
    return <span>-</span>;
  }
};
/** 金额输入 */
function CurrencyInput({ hightLight, className, ...restProps }: CurrencyInputProps) {
  return (
    <InputNumber
      formatter={(value) => {
        return formatter(value)?.toString() ?? '';
      }}
      parser={(value) => {
        return parser(value) ?? '';
      }}
      className={`w-full currency ${hightLight ? 'text-primary' : ''} ${className}`}
      {...restProps}
    />
  );
}
CurrencyInput.defaultProps = { min: 0 };

Currency.formatter = formatter;
Currency.parser = parser;
Currency.Input = CurrencyInput;
export default Currency;
