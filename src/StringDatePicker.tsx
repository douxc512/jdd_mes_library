import React, { useCallback, useEffect, useState } from 'react';
import { DatePicker } from 'antd';
import dayjs, { Dayjs } from 'dayjs';

// 系统中提交的日期都是以string传递的，故封装组件将string转为dayjs，将选中的日期转为string

interface Props {
  format: string;
  value?: string;
  /** 支持清除，所以会传undefined */
  onChange?: (v: string | undefined) => void;
  className?: string;
  disabled?: boolean;
  disabledDate?: (date: any) => boolean;
}
const StringDatePicker = function ({ value, onChange, format, ...restProps }: Props) {
  const [date, setDate] = useState<Dayjs | undefined>();
  useEffect(() => {
    setDate(value ? dayjs(value) : undefined);
  }, [value]);
  const handleChange = useCallback(
    (e: any) => {
      if (e === null || e === undefined) {
        onChange?.(e);
      } else {
        onChange?.(e.format(format));
      }
    },
    [onChange],
  );
  return <DatePicker value={date as any} onChange={handleChange} format={format} {...restProps} />;
};

StringDatePicker.defaultProps = {
  format: 'YYYY-MM-DD',
};
export default StringDatePicker;
