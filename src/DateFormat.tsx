import React from 'react';
import { valueNotNullAndUndefined } from './utils';
import dayjs, { Dayjs } from 'dayjs';

// 日期格式化
export default function DateFormat({ value, format }: { value?: string | Dayjs | null | undefined; format?: string }) {
  if (valueNotNullAndUndefined(value)) {
    return <span>{dayjs(value).format(format ?? 'YYYY-MM-DD')}</span>;
  } else {
    return <span>-</span>;
  }
}
