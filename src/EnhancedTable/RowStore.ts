import { useEffect, useState } from 'react';

/**
 * 存储所有已选的rows
 */
type RowProps = Record<string, any>;
interface Operate {
  setRow: (v: RowProps) => void;
  getRow: (v: string) => RowProps | undefined;
  getRows: () => RowProps[];
  getKeys: () => string[];
  isContain: (v: RowProps) => boolean;
  removeRows: (v: string[]) => void;
  clear: () => void;
}
export default function useRowStore(uniqueKey: string): [RowProps[], Operate] {
  const [rows] = useState<Map<string, RowProps>>(new Map());
  const [allRows, setAllRows] = useState<RowProps[]>([]);
  // 组件销毁时清空
  // 如后续需要支持跨页面选择保存等，可以修改此处逻辑
  useEffect(() => {
    return function clearRowStore() {
      rows.clear();
    };
  }, []);
  const operates: Operate = {
    setRow(row) {
      rows.set(row[uniqueKey], row);
      setAllRows(Array.from(rows.values()));
    },
    getRow(key) {
      return rows.get(key);
    },
    getRows() {
      return Array.from(rows.values());
    },
    getKeys() {
      return Array.from(rows.keys());
    },
    isContain(row) {
      return rows.has(row[uniqueKey]);
    },
    /** 删除指定的items */
    removeRows(keys) {
      keys.forEach((key) => {
        rows.delete(key);
      });
      setAllRows(Array.from(rows.values()));
    },
    clear() {
      rows.clear();
      setAllRows([]);
    },
  };
  return [allRows, operates];
}
