import React, { forwardRef, Key, ReactNode, useCallback, useEffect, useImperativeHandle, useState } from 'react';
import { SettingOutlined } from '@ant-design/icons';
import {
  Col,
  Pagination,
  Row,
  Table,
  TablePaginationConfig,
  TableProps,
  TableColumnGroupType,
  TableColumnType,
} from 'antd';
import { valueNotNullAndUndefined } from '../utils';
import ColumnSetting, { shouldReturnItem } from './ColumnSetting';
import DashCell from './DashCell';
import EnhancedTableFooter from './Footer';
import useRowStore from './RowStore';

export type RecordType = any;
export type TableColumn<RecordType = any> = (TableColumnType<RecordType> | TableColumnGroupType<RecordType>) & {
  title: string | JSX.Element;
  summary?: ReactNode;
  showInSelect?: boolean;
  children?: TableColumn[] | null;
  dataIndex?: string | number | readonly (string | number)[];
};

interface EnhancedTableProps<RecordType> extends Omit<TableProps<RecordType>, 'columns' | 'pagination'> {
  columns: TableColumn[];
  saveKey: string;
  pagination?: TablePaginationConfig | null;
  /** 表格循环的key，也用作rowSelection重复判断 */
  rowKey: string;
}

/**
 * 增强版的table，基于Ant Design table
 * 1、支持配置column是否展示
 * 2、支持column中配置summary显示在底部，和column对应
 * 3、支持跨页选择
 */
const EnhancedTable = forwardRef<{ clearRowSelection: () => void }, EnhancedTableProps<RecordType>>(
  ({ pagination, rowSelection, size, columns, saveKey, rowKey, dataSource, ...rest }, ref) => {
    // 对外提供清空已选item方法
    useImperativeHandle(ref, () => {
      return {
        clearRowSelection: function () {
          rowStore.clear();
          setSelectedKeys([]);
          rowSelection?.onChange?.([], [], { type: 'multiple' });
        },
      };
    });
    // rowKey用于指定保存时使用的唯一key
    const [rows, rowStore] = useRowStore(rowKey);
    // 是否展示表格column设置
    const [showSetting, setShowSetting] = useState<boolean>(false);
    const [showColumnSetting, setShowColumnSetting] = useState<boolean>(false);
    // columns
    const [clonedColumns, setClonedColumns] = useState<TableColumn[]>([]);
    // 判断本地是否有配置项，如果有则从本地还原，否则展示全部
    useEffect(() => {
      try {
        const localConfig = localStorage.getItem(`${saveKey}_columns`);
        if (valueNotNullAndUndefined(localConfig)) {
          // 仅在弹窗不可见时处理columns
          const keys = JSON.parse(localConfig!);
          setClonedColumns(shouldReturnItem(columns, keys!) ?? []);
        } else {
          setClonedColumns(columns);
        }
      } catch (error) {
        console.error(error);
        setClonedColumns(columns);
      }
    }, [columns]);
    const currentPage = pagination?.current ?? 1;
    // 当前页面选中的key
    const [selectedKeys, setSelectedKeys] = useState<Key[]>([]);
    // 默认页面数据变化时，从rowStore中计算一次当前页面选中的数据
    useEffect(() => {
      const originRowSelectionChange = rowSelection?.onChange;
      const defaultSelectedRows = dataSource?.filter(rowStore.isContain);
      if (defaultSelectedRows && defaultSelectedRows.length > 0) {
        const keys = defaultSelectedRows?.map((item) => item?.[rowKey]);
        setSelectedKeys(keys);
        originRowSelectionChange?.(rowStore.getKeys(), rowStore.getRows(), { type: 'multiple' });
      }
    }, [dataSource]);

    const clonedRowSelection = useCallback(() => {
      if (valueNotNullAndUndefined(rowSelection)) {
        const originRowSelectionChange = rowSelection!.onChange;

        return {
          ...rowSelection,
          selectedRowKeys: selectedKeys,
          onChange(keys: Key[], rows: RecordType[], info: any) {
            let diffKeys: Key[] = [];
            // 找出当前页面上差异的key，必须是当前页面的数据，且不是被选中的
            // 先找出不是当前选中的key
            const keysNotInCurrentSelectedKeys = selectedKeys.filter((originKey) => keys.indexOf(originKey) === -1);
            diffKeys = keysNotInCurrentSelectedKeys.filter(
              (key) => dataSource?.findIndex((data) => data[rowKey] === key) !== -1,
            );
            setSelectedKeys(keys);
            // 删除当前页面上移除的选项
            rowStore.removeRows(diffKeys as string[]);
            // 将选择的row统一保存
            rows.forEach((row) => {
              rowStore.setRow(row);
            });
            originRowSelectionChange?.(rowStore.getKeys(), rowStore.getRows(), info);
          },
        };
      } else {
        return rowSelection;
      }
    }, [rowSelection, currentPage, selectedKeys, rowKey, dataSource]);
    // 根据传入的统计数据，生成底部统计
    const renderSummary = useCallback(() => {
      // 统计的字段和column是对应的，所以从clonedColumn中匹配应该放在哪个位置
      // clonedColumn存在children，所以先展开成一层，然后计算
      let spreadColumns: any[] = [];
      clonedColumns?.forEach((column) => {
        if (column.children && column.children.length > 0) {
          spreadColumns = spreadColumns.concat(column.children);
        } else {
          spreadColumns.push(column);
        }
      });
      // 如果展开之后的column中没有配置过summary，则全部不用展示
      const shouldHideSummary = spreadColumns.findIndex((column) => valueNotNullAndUndefined(column.summary)) === -1;
      if (shouldHideSummary) {
        return null;
      }
      // 正常展示
      const summaryList = spreadColumns.map((column, index) => {
        let cellIndex = index;
        if (rowSelection) {
          // 有左侧的checkbox，则index需要+1
          cellIndex = index + 1;
        }
        if (valueNotNullAndUndefined(column.summary)) {
          return (
            <Table.Summary.Cell align="center" key={`summary_cell_${cellIndex}`} index={cellIndex}>
              {column.summary}
            </Table.Summary.Cell>
          );
        } else {
          return (
            <Table.Summary.Cell key={`summary_cell_${cellIndex}`} index={cellIndex}>
              {cellIndex === 0 ? '总计' : null}
            </Table.Summary.Cell>
          );
        }
      });
      return (
        <Table.Summary fixed>
          <Table.Summary.Row>
            {rowSelection ? (
              <Table.Summary.Cell key={`summary_cell_${0}`} index={0}>
                总计
              </Table.Summary.Cell>
            ) : null}
            {summaryList}
          </Table.Summary.Row>
        </Table.Summary>
      );
    }, [clonedColumns, rowSelection]);
    return (
      <div className="relative">
        {showSetting ? (
          <span
            className="absolute right-5px -top-22px text-black/45 animate-zoom-in"
            onClick={() => {
              setShowColumnSetting(true);
            }}
          >
            <SettingOutlined />
          </span>
        ) : null}
        <Table
          rowKey={rowKey}
          onHeaderRow={() => {
            return {
              onMouseEnter() {
                setShowSetting(true);
              },
            };
          }}
          dataSource={dataSource}
          components={{ body: { cell: DashCell } }}
          size={size}
          columns={clonedColumns}
          rowSelection={clonedRowSelection()}
          pagination={false}
          summary={() => {
            return renderSummary();
          }}
          {...rest}
        />
        <Row justify="space-between" align="middle" style={{ marginTop: 16 }}>
          <Col>
            <EnhancedTableFooter
              rowKey={rowKey}
              originColumns={clonedColumns}
              selectedRows={rows}
              onDelete={(record) => {
                // 删除某条已选中的数据
                setSelectedKeys((prev) => {
                  return prev.filter((key) => key !== record[rowKey]);
                });
                rowStore.removeRows([record[rowKey]]);
              }}
              onClear={() => {
                setSelectedKeys([]);
                rowStore.clear();
              }}
            />
          </Col>
          <Col>
            {valueNotNullAndUndefined(pagination) ? (
              <Pagination size="default" showSizeChanger showQuickJumper {...pagination} />
            ) : null}
          </Col>
        </Row>
        <ColumnSetting
          saveKey={saveKey}
          rowKey={rowKey}
          originColumns={columns}
          open={showColumnSetting}
          onOk={(columns) => {
            setClonedColumns(columns);
            setShowSetting(false);
            setShowColumnSetting(false);
          }}
          onCancel={() => {
            setShowSetting(false);
            setShowColumnSetting(false);
          }}
        />
      </div>
    );
  },
);

EnhancedTable.defaultProps = {
  size: 'small',
};
export default EnhancedTable;
