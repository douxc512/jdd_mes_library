import { TreeProps, Row, Col, Tree, Button, ModalProps, Modal } from 'antd';
import React, { useState, useEffect, Key } from 'react';
import { valueNotNullAndUndefined } from '../utils';
import { TableColumn } from './index';

function getAllKeys(items: Record<string, any>[]) {
  let ret: string[] = [];
  items.forEach((item) => {
    ret.push(item.key);
    if (item.children) {
      ret = ret.concat(getAllKeys(item.children));
    }
  });
  return ret;
}
/**
 * 给每一个item添加key
 * @param items
 * @returns
 */
function setKey(items: TableColumn[]): (TableColumn & { disabled?: boolean })[] {
  return items.map((item) => {
    if (item.children) {
      item.children = setKey(item.children);
    }
    item.key = `${item.title}_${item.dataIndex ?? '-'}`;
    return item;
  });
}

export function shouldReturnItem(items: TableColumn[] | undefined, names: Key[]) {
  if (items === undefined) {
    return items;
  }
  const result: TableColumn[] = [];
  items.forEach((item) => {
    // 先判断children是否有选中的
    const children = valueNotNullAndUndefined(item.children) ? shouldReturnItem(item.children!, names) : null;
    if (names.indexOf(`${item.title}_${item.dataIndex ?? '-'}`) !== -1 || (children && children?.length > 0)) {
      result.push({ ...item, children });
    }
  });
  return result;
}
/**
 * Table column配置，控制column展示或者隐藏
 */
function ColumnTree({
  onKeysChange,
  saveKey,
  ...restProps
}: TreeProps & { saveKey: string; onKeysChange: (v: string[]) => void }) {
  const [checkedKeys, setCheckedKeys] = useState<string[]>();
  useEffect(() => {
    const localConfig = localStorage.getItem(`${saveKey}_columns`);
    let keys: string[] | undefined;
    if (valueNotNullAndUndefined(localConfig)) {
      keys = JSON.parse(localConfig!);
    } else {
      // 本地没有配置，则是默认全部
      keys = restProps.treeData ? getAllKeys(restProps.treeData) : undefined;
    }
    onKeysChange(keys!);
    setCheckedKeys(keys);
  }, []);
  return (
    <div className="max-h-500px overflow-y-auto">
      <Row gutter={16} className="w-full">
        <Col span={12}>
          <Tree
            checkable
            checkedKeys={checkedKeys}
            defaultExpandAll
            fieldNames={{ key: 'key', title: 'title', children: 'children' }}
            onCheck={(keys, e) => {
              const allKey = e.checkedNodes.map((node) => node.key);
              setCheckedKeys(allKey as string[]);
              onKeysChange?.(allKey as string[]);
            }}
            {...restProps}
          />
        </Col>
        <Col span={12} className="text-right">
          <Button
            type="primary"
            size="small"
            onClick={() => {
              if (restProps.treeData) {
                setCheckedKeys(getAllKeys(restProps.treeData));
                onKeysChange(getAllKeys(restProps.treeData));
              } else {
                setCheckedKeys(undefined);
                onKeysChange([]);
              }
            }}
          >
            全选
          </Button>
        </Col>
      </Row>
    </div>
  );
}

interface ColumnSettingProps extends Omit<ModalProps, 'onOk'> {
  originColumns: any[];
  saveKey: string;
  rowKey: string;
  onOk?: (v: TableColumn[]) => void;
}
/** 表格column配置 */
export default function ColumnSetting({ open, saveKey, rowKey, originColumns, onOk, ...rest }: ColumnSettingProps) {
  const [checkedKeys, setCheckedKeys] = useState<Key[]>();
  return (
    <Modal
      title="设置"
      open={open}
      centered
      destroyOnClose
      width="480px"
      onOk={() => {
        if (checkedKeys && checkedKeys.length > 0) {
          localStorage.setItem(`${saveKey}_columns`, JSON.stringify(checkedKeys));
          onOk?.(shouldReturnItem(originColumns, checkedKeys) ?? []);
        }
      }}
      {...rest}
    >
      <ColumnTree
        treeData={
          setKey(originColumns).map((column) => {
            // 被设置为rowKey的字段，不能取消勾选，必须展示，否则table展示渲染会出错
            column.disabled = column.dataIndex === rowKey;
            return { ...column };
          }) as any[]
        }
        saveKey={saveKey}
        onKeysChange={(keys) => {
          setCheckedKeys(keys);
        }}
      />
    </Modal>
  );
}
