/** 自定义Table的一些组件 */
import React from 'react';
import { Typography } from 'antd';

/** 判断如果没有值显示 -
 * 如果有值，且只有string，则换行时左对齐
 */
export default function DashCell({ children, ...restProps }: any) {
  function render() {
    const value = children?.[children.length - 1];
    if (value === null || value === undefined) {
      return '-';
    }
    // 不再支持折行了，超出部分...，鼠标移入显示全部
    if (typeof value === 'string' || typeof value === 'number') {
      let text = children;
      // 如果第一个是react组件，不用在tooltip中展示
      if (children[0]?.type?.toString() === 'Symbol(react.fragment)') {
        text = children[children.length - 1];
      }
      return (
        <Typography.Text style={{ maxWidth: '100%', color: 'rgba(0,0,0,0.65)' }} ellipsis={{ tooltip: text }}>
          {children}
        </Typography.Text>
      );
    }
    return children;
  }
  return <td {...restProps}>{render()}</td>;
}
