// 底部Footer，展示 已选x条数据，点击后弹出所有已选数据

import { InfoCircleFilled } from '@ant-design/icons';
import { Button, Modal, Space, Table } from 'antd';
import React, { useState, useEffect } from 'react';
import DashCell from './DashCell';
import { RecordType, TableColumn } from './index';

interface EnhancedTableFooterProps {
  selectedRows: RecordType[];
  originColumns: TableColumn[];
  rowKey: string;
  onDelete: (record: RecordType) => void;
  // 清空所有
  onClear: () => void;
}
export default function EnhancedTableFooter({
  selectedRows,
  originColumns,
  rowKey,
  onDelete,
  onClear,
}: EnhancedTableFooterProps) {
  const [parsedColumns, setParsedColumns] = useState<TableColumn[]>([]);
  useEffect(() => {
    setParsedColumns(originColumns.filter((column) => column.showInSelect || column.dataIndex === rowKey));
  }, [originColumns]);
  const [visible, setVisible] = useState(false);
  return selectedRows.length > 0 ? (
    <>
      <Button
        size="small"
        type="text"
        onClick={() => {
          setVisible(true);
        }}
      >
        <InfoCircleFilled style={{ color: '#1989fa' }} />
        <span className="text-black/65 ml-5">
          已选
          <span className="text-primary"> {selectedRows.length} </span>
          条数据
        </span>
      </Button>
      <Modal
        title="已选数据"
        open={visible}
        width={780}
        destroyOnClose
        centered
        footer={
          <Space>
            <Button
              onClick={() => {
                setVisible(false);
                onClear?.();
              }}
            >
              清空
            </Button>
            <Button
              type="primary"
              onClick={() => {
                setVisible(false);
              }}
            >
              确定
            </Button>
          </Space>
        }
        onCancel={() => {
          setVisible(false);
        }}
        onOk={() => {
          setVisible(false);
        }}
      >
        <Table
          rowKey={rowKey}
          size={'small'}
          components={{ body: { cell: DashCell } }}
          scroll={{ y: 500 }}
          columns={[
            ...parsedColumns,
            {
              title: '操作',
              dataIndex: 'operate',
              render(val, record) {
                return (
                  <Button
                    danger
                    type="text"
                    onClick={() => {
                      onDelete?.(record);
                    }}
                  >
                    删除
                  </Button>
                );
              },
            },
          ]}
          dataSource={visible ? selectedRows : []}
          pagination={false}
        />
      </Modal>
    </>
  ) : null;
}
