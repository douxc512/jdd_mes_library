import './index.css';
import React from 'react';
import { Button, Input, Modal, ModalProps, Space, Steps, Typography } from 'antd';
import { useCallback, useState } from 'react';

// 批量导入弹窗模版
interface Props extends ModalProps {
  /** 下载模版 */
  onDownLoadTemplate: () => void;
  upload: (file: File) => Promise<void>;
}
const ImportModal = function ({ onOk, onCancel, upload, onDownLoadTemplate, ...restProps }: Props) {
  const [file, setFile] = useState<File | null>(null);
  const [errors, setErrors] = useState<string[]>([]);
  const [loading, setLoading] = useState(false);

  const [formError, setFormError] = useState<string | null>(null);
  return (
    <Modal
      confirmLoading={loading}
      destroyOnClose
      okText="提交"
      onCancel={(e) => {
        setErrors([]);
        setFormError(null);
        setFile(null);
        setLoading(false);
        onCancel?.(e);
      }}
      onOk={async (e) => {
        try {
          setLoading(true);
          if (file === null) {
            setFormError('请选择要上传的文件');
            return;
          }
          await upload?.(file as File);
          onOk?.(e);
        } catch (error: any) {
          console.error(error);
          if (error.data) {
            if (typeof error.data === 'string') {
              setErrors([error.data]);
            } else {
              setErrors(error.data.map((item: any) => `${item.lineDescribe}${item.describe}`));
            }
          }
        } finally {
          setLoading(false);
        }
      }}
      {...restProps}
    >
      <Steps direction="vertical">
        <Steps.Step
          status="process"
          title="下载模版"
          description={
            <>
              <div className="import-modal-tips">下载模版后，录入材质数据</div>
              <Button
                onClick={() => {
                  onDownLoadTemplate?.();
                }}
              >
                下载模版
              </Button>
            </>
          }
        />
        <Steps.Step
          status="process"
          title="选择Excel"
          description={
            <>
              <div className="import-modal-tips">将下载的模板中录入数据，保存好后上传</div>
              <Space>
                <Input readOnly value={file?.name} status={formError ? 'error' : ''} />
                <Button
                  onClick={() => {
                    const input = document.createElement('input');
                    input.type = 'file';
                    input.accept =
                      '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel';
                    input.multiple = false;
                    input.onchange = function () {
                      const files = input.files?.[0];
                      if (files) {
                        setFormError(null);
                        setFile(files);
                      }
                    };
                    input.click();
                  }}
                >
                  选择Excel
                </Button>
              </Space>
              <div>{formError && <Typography.Text type="danger">{formError}</Typography.Text>}</div>
              <ErrorList data={errors} visible={errors.length > 0} />
            </>
          }
        />
      </Steps>
    </Modal>
  );
};

export default ImportModal;

// 错误
interface ErrorListProps {
  data: string[];
  visible: boolean;
}
function ErrorList({ data, visible }: ErrorListProps) {
  const renderError = useCallback(() => {
    return data.map((error) => <li key={error}>{error}</li>);
  }, [data]);
  return visible ? (
    <div className="import-modal-error">
      <div className="import-modal-error-title">
        数据错误（<Typography.Text type="danger">{data.length}</Typography.Text>）：
      </div>
      <ul className="import-modal-error-list">{renderError()}</ul>
    </div>
  ) : null;
}
